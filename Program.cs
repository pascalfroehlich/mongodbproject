﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDB_Sample
{
  class Program
  {
    static void Main(string[] args)
    {
      var wrapper = new MongoDBLib.MongoDBWrapper();

      var count = wrapper.GetRestaurantCount();

      Console.WriteLine(string.Format("Number of restaurant: {0}", count));

      var names = wrapper.GetRestaurantNames();
      foreach (var name in names)
      {
        Console.WriteLine(name);
      }

      Console.WriteLine("Press ENTER to exit application");
      Console.ReadLine();
    }
  }
}
