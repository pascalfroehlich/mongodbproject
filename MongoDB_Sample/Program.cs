﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MongoDB_Sample
{
    class Program
    {
        static void Main(string[] args)
        {

            //Das Programm
            var wrapper = new MongoDBLib.MongoDBWrapper();

            var countVeranstaltung = wrapper.GetVeranstaltungCount();
            var countProfil = wrapper.GetUserCount();

            Console.WriteLine(string.Format("Number of Veranstaltungen: {0}", countVeranstaltung));

            Console.WriteLine("-------------------------------");
            Console.WriteLine("Verantaltungen: \n");

            var namesVer = wrapper.GetVeranstaltungNames();
            foreach (var name in namesVer)
            {
                Console.WriteLine(name);
            }

            /*Console.WriteLine("-------------------------------");
            Console.WriteLine("Profile: \n");

                    var namesProfil = wrapper.GetProfilNames();
                    foreach (var name in namesProfil)
                    {
                        Console.WriteLine(name);
                    }
                    */

            Console.WriteLine("-------------------------------");
            Console.Write("InsertProfil: ");
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("* \n");
            Thread.Sleep(300);

            wrapper.InsertProfil();

            Console.WriteLine("-------------------------------");
            Console.Write("InsertVeranstaltung:");
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("* \n");
            Thread.Sleep(300 );

            wrapper.GetInsertVeranstaltung();

            Console.WriteLine("-------------------------------");
            Console.Write("InsertBeitrag:");
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("* \n");
            Thread.Sleep(300);

            wrapper.GetInsertBeitrag();

            Console.WriteLine("-------------------------------");
            Console.Write("Delete Profil:");
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("* \n");
            Thread.Sleep(300);

            wrapper.deleteProfil();


            Console.WriteLine("-------------------------------");
            Console.WriteLine("Email Profile: \n");
            Console.WriteLine(string.Format("Number of Profil: {0}", countProfil));

            Console.WriteLine("-------------------------------");
            Console.WriteLine("Email Profile: \n");

            var mails = wrapper.GetProfilMail();
            foreach (var mail in mails)
            {
                Console.WriteLine(mail);
            }

            Console.WriteLine("-------------------------------");
            Console.WriteLine("Email Profile: \n");


            Console.WriteLine("" + wrapper.sucheName("asdf99@hotmail.de"));


            Console.WriteLine("-------------------------------");
            Console.Write("InsertLeistungen: ");
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("*");
            Thread.Sleep(300);
            Console.Write("* \n");
            Thread.Sleep(300);

            wrapper.InsertLeistung();


            Console.WriteLine("Press ENTER to exit application");
            Console.ReadLine();
        }
    }
}
