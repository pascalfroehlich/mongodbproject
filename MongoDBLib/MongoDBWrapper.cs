﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDBLib;

namespace MongoDBLib
{
    public class MongoDBWrapper
    {
        private IMongoClient client;
        private IMongoDatabase db;

        public MongoDBWrapper()
        {
            var settings = new MongoClientSettings()
            {//Settings für MongoDB nach der IP Adresse
             // Check euren Port ob der gleich ist
                Server = new MongoServerAddress("localhost", 27017
                 //Properties.Settings.Default.MongoDBHost,
                 //Properties.Settings.Default.MongoDBPort
                 )
            };

            this.client = new MongoClient(settings);
            //Datenbank Name
            //!!!!So muss eure DB auch heißen damit es Funktioniert!!!!!!!
            this.db = client.GetDatabase("MongoDBProject");
        }

        public long GetVeranstaltungCount()
        {
            var filter = new BsonDocument();
            var result = this.db.GetCollection<BsonDocument>("veranstaltung").Count(filter);
            return result;
        }

        public IList<string> GetVeranstaltungNames()
        {
            var filter = new BsonDocument();
            var result =
              this.db.GetCollection<BsonDocument>("veranstaltung")
              .Find(filter).ToList()
              .Select(o => o.GetValue("name").AsString).ToList();

            return result;
        }

        public IList<string> GetProfilMail()
        {
            var filter = new BsonDocument();
            var result =
              this.db.GetCollection<BsonDocument>("profil")
              .Find(filter).ToList()
              .Select(o => o.GetValue("emailAdresse").AsString).ToList();

            return result;
        }

        public long GetUserCount()
        {
            var filter = new BsonDocument();
            var result = this.db.GetCollection<BsonDocument>("profil").Count(filter);
            return result;
        }

        /*public IList<string> GetProfilNames()
        {
            var filter = new BsonDocument();
            var result =
              this.db.GetCollection<BsonDocument>("profil")
              .Find(filter).ToList()
              .Select(o => o.GetValue("ObjectId").AsString).ToList();

            return result;

        }
        */

        //Insert Beitrag
        public void GetInsertBeitrag()
        {

            var document = new BsonDocument
            {
               
                    { "text", "Hey Leute :)" },
                    { "zeitstempel", new BsonArray { 2015, 2, 7, 20, 25 } }

            };


            this.db.GetCollection<BsonDocument>("beitrag").InsertOneAsync(document);
        }
        //Insert-Veranstaltung
        public void GetInsertVeranstaltung()
        {
            var document = new BsonDocument
            {


                        { "veranstaltungsID", "10" },
                        { "name", "Schaumparty" },
                        { "beschreibung", "Die beste Schaumparty von Wien" },
                        { "anfang", "13.10.2017" },
                        { "ende", "14.10.2017" }

                };

                
            this.db.GetCollection<BsonDocument>("veranstaltung").InsertOneAsync(document);
        }


        //Insert-Profil
        public void InsertProfil()
        {
            var document = new BsonDocument {

                        { "profil_id", 50 },
                        { "beziehnung", "willig" },
                        { "passwort", "test123456" },
                        { "emailAdresse", "test123@hotmail.com" }
                };
            


            this.db.GetCollection<BsonDocument>("profil").InsertOneAsync(document);
        }


        public void deleteProfil()
        {
            var filter = new BsonDocument("profil_id", 2);
            var arg = this.db.GetCollection<BsonDocument>("profil").FindOneAndDelete(filter);

        }

        

        public Boolean sucheName(string name)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("emailAdresse", name);
            var t = this.db.GetCollection<BsonDocument>("profil")
                  .Find(filter).ToList();
            Console.WriteLine("" + t.First().ToString());
            return true;
        }


        public void InsertLeistung()
        {
            var document = new BsonDocument {

                        { "bezeichnung", "Fußballtraining"},
                        { "firmen_id", 80},
                     
                };



            this.db.GetCollection<BsonDocument>("leistungen").InsertOneAsync(document);
        }




    }



    }

